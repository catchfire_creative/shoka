/*
------------------------------

Shoka Gulpfile

Shoka uses The Laravel Elixir Gulp API Wrapper
Additional Information: https://laravel.com/docs/5.3/elixir

Available Commands:

- gulp
-- Runs all tasks 

- gulp --production
-- Runs all tasks and minifys JS and CSS

- gulp watch
-- Watches SCSS and JS files for changes and runs all tasks on change

------------------------------
 */

/**
 * Get Elixir
 */
const elixir = require('laravel-elixir');

/**
 * Plugins
 */
require('laravel-elixir-imagemin');
require('laravel-elixir-svg-symbols');

/**
 * Set Project Paths
 */
elixir.config.assetsPath = './assets/src';
elixir.config.publicPath = './assets';

elixir((mix) => {

  /**
   * SASS Build Task
   */
  mix.sass('main.scss', 'assets/shop.css');

  /**
   * JS Build Task
   */
  mix.webpack('main.js', 'assets/shop.js');

  /**
   * Images Task
   */
  mix.imagemin();

  /**
   * SVG Task
   */
  mix.svgSprite('assets/src/svg', 'assets', {
    mode: {
      symbol: {
        sprite: 'svg-symbols.liquid',
        dest: '.'
      }
    },
    svg: {
      xmlDeclaration: false,
      rootAttributes: {
        'style': 'display: none'
      },
      namespaceClassnames: false
    }  
  });

  /**
   * Copy Task
   * -- Fonts
   */
  mix.copy('assets/src/fonts', 'assets');

});