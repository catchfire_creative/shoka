Shoka Shopify Starter Theme
============

The Shoka theme is a simplified Shopify theme, to be used as a "blank slate" starting point for themes.

**Demo:**

- [Demo store](https://shoka-demo.myshopify.com)

Getting started
---------------------
1. Make sure you have the [Shopify Theme Kit](https://shopify.github.io/themekit/) installed
2. Setup [API access](http://shopify.github.io/themekit/#get-api-access) to the development store.
2. Create a direcotry for your theme and `cd` into it.
3. Run `theme bootstrap --password=[your-api-password] --store=[your-store.myshopify.com] --name=[your-theme-name] --url=[https://bitbucket.org/catchfire_creative/shoka/get/HEAD.zip]`
4. Run `yarn install` or `npm i` to install build dependencies



Additional resources
---------------------
- [Theme Documentation](http://docs.shopify.com/themes): Learn more about Liquid and theme templates.
